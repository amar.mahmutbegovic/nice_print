#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <array>
#include <cstring>
#include <iostream>
#include <string>

#include "nice_print.hpp"

class NicePrintTest : public ::testing::Test {
   protected:
    static inline std::array<char, 512> buffer = {0};
    static inline std::size_t cnt = 0;

    void SetUp() override {
        memset(begin(buffer), 0, sizeof(buffer));
        cnt = 0;
    }

   public:
    static void print_buffer_impl(const char* buff, std::size_t len) {
        for (int i = 0; i < len; i++) {
            if (buff[i] != '\0') buffer.at(cnt++) = buff[i];
        }
    }
    using np = nice_print<print_buffer_impl>;
};

TEST_F(NicePrintTest, hexOutputTest) {
    np::print("Hello world!", np::nl, "This is a hex number ", np::hex(256),
              np::nl);

    std::string expected_str("Hello world!\r\nThis is a hex number 0x100\r\n");
    std::string buffer_str(begin(buffer), cnt);

    EXPECT_TRUE(expected_str == buffer_str);
}

TEST_F(NicePrintTest, floatOutputTest) {
    np::print("This is a float ", 2.23f, np::nl);

    std::string expected_str("This is a float 2.23\r\n");
    std::string buffer_str(begin(buffer), cnt);

    EXPECT_TRUE(expected_str == buffer_str);
}
