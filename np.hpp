#pragma once

#include <cstdio>

#include "nice_print.hpp"

inline void print_buffer_implementation(const char *buff, std::size_t len) {
    for (int i = 0; i < len; i++) {
        fputc(buff[i], stdout);
    }
}

using np = nice_print<print_buffer_implementation>;
