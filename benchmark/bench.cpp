#include <benchmark/benchmark.h>

#include "np.hpp"

int a = 0xABCD;
int b = 22;
float c = 55.66;

using namespace std;

static void BM_print(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    np::print("a = ", np::hex(a), np::nl, "b = ", b, "c = ", c, np::nl);
  }
}

static void BM_printf(benchmark::State &state) {
  // Perform setup here
  for (auto _ : state) {
    // This code gets timed
    fprintf(stdout, "a = 0x%.4x\r\nb = %d\r\nc = %0.2f\r\n", a, b, c);
  }
}

// Register the function as a benchmark
BENCHMARK(BM_printf)->Unit(benchmark::kMicrosecond);
BENCHMARK(BM_print)->Unit(benchmark::kMicrosecond);

BENCHMARK_MAIN();
