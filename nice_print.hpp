#pragma once

#include <charconv>
#include <cmath>
#include <cstring>

#include "boost/boost/fusion/include/adapt_struct.hpp"
#include "boost/boost/fusion/include/is_sequence.hpp"
#include "boost/boost/fusion/include/sequence.hpp"

typedef void (*print_buff_func_t)(const char *buff, std::size_t len);

template <print_buff_func_t print_buff> struct nice_print {

  template <typename T> struct hex {
    T value;
    hex(T t) { value = t; }
  };

  template <typename T> struct bin {
    T value;
    bin(T t) { value = t; }
  };

  static constexpr char nl[] = "\r\n";

  template <std::size_t N> static void print_impl(const char (&c_str)[N]) {
    print_buff(c_str, N);
  }

  static void print_impl(int val) {
    char buff[32];
    if (auto [ptr, ecc] = std::to_chars(buff, &buff[32 - 1], val);
        ecc == std::errc()) {
      buff[ptr - buff] = '\0';
      print_buff(buff, ptr - buff);
    }
  }

#ifdef CHARCONV_HAS_FLOAT
  static void print_impl(float val) {
    char buff[32];
    if (auto [ptr, ecc] =
            std::to_chars(buff, &buff[32 - 1], val, std::chars_format::general);
        ecc == std::errc()) {
      buff[ptr - buff] = '\0';
      print_buff(buff, ptr - buff);
    }
  }
#else
  static void print_impl(float val) {
    print_impl(static_cast<int>(val));
    print_impl(".");
    print_impl(
        static_cast<int>(std::floor(100 * (val - static_cast<int>(val)))));
  }
#endif

  static void print_impl(hex<int> val) {
    print_impl("0x");
    char buff[32];
    if (auto [ptr, ecc] = std::to_chars(buff, &buff[32 - 1], val.value, 16);
        ecc == std::errc()) {
      buff[ptr - buff] = '\0';
      print_buff(buff, ptr - buff);
    }
  }

  static void print_impl(bin<int> val) {
    print_impl("0b");
    char buff[32];
    if (auto [ptr, ecc] = std::to_chars(buff, &buff[32 - 1], val.value, 2);
        ecc == std::errc()) {
      buff[ptr - buff] = '\0';
      print_buff(buff, ptr - buff);
    }
  }

  template <typename T>
  using is_adapted_struct =
      std::is_same<typename boost::fusion::traits::tag_of<T>::type,
                   boost::fusion::struct_tag>;

  struct print_adapted_struct_member {
    template <typename T> void operator()(const T &val) const {
      print_impl(val);
      print_impl(nl);
    }
  };

  // last recursive call - zero arguments
  static void print() {}

  template <typename T, typename... Ts>
  static void print(T &&first, Ts &&...rest) {
    if constexpr (is_adapted_struct<typename std::remove_reference<decltype(
                      first)>::type>::value) {
      boost::fusion::for_each(first, (print_adapted_struct_member{}));
    } else {
      print_impl(first);
    }
    print(rest...);
  }
};
