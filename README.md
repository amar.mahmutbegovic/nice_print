# Nice print
- C++17 header file library for python-like print function.
- Embedded target oriented - lightweight with basic formatting capability.
- No exceptions, RTTI and dynamic allocation.

## Configuration
- Pass buffer printing function as template argument to nice_print structure. 
- Example implementation in np.hpp:

```c++17
inline void print_buffer_implementation(const char *buff, std::size_t len) {
    for (int i = 0; i < len; i++) {
        fputc(buff[i], stdout);
    }
}
using np = nice_print<print_buffer_implementation>;
```
## Basic usage
```c++17
#include "np.hpp"
...
np::print("Hello world!", np::nl, "This is a hex number ", np::hex(256), np::nl);
np::print("And this is the same number in binary form ", np::bin(256), np::nl);
np::print("This is a float ", 2.23f, np::nl);
 ```
Output
```
Hello world!
This is a hex number 0x100
And this is the same number in binary form 0b100000000
This is a float 2.23
```
## Print boost fusion adapted struct 
- Basic support for boost fusion adapted struct.
```c++17
struct GyroData {
  int x;
  int y;
  float z;
};
BOOST_FUSION_ADAPT_STRUCT(GyroData, (int, x)(int, y)(float, z))
...
const GyroData gyro_data = {44, 55, 0.36};
np::print("Printing gyro data struct: ", np::nl, gyro_data);

```
Output
```
Printing gyro data struct: 
44
55
0.36

```
