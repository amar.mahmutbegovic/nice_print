#pragma once

#include <boost/boost/fusion/include/adapt_struct.hpp>

struct GyroData {
  int x;
  int y;
  float z;

  void print() const;
};

BOOST_FUSION_ADAPT_STRUCT(GyroData, (int, x)(int, y)(float, z))


