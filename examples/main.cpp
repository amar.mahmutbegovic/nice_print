#include <cstdint>

#include "np.hpp"
#include "sensor.h"

int main() {

  np::print("Hello world!", np::nl, "This is a hex number ", np::hex(256),
            np::nl);
  np::print("And this is the same number in binary form ", np::bin(256),
            np::nl);

  np::print("This is a float ", 2.23f, np::nl);

  const GyroData gyro_data = {44, 55, 0.36};
  gyro_data.print();
  np::print("Printing gyro data struct: ", np::nl, gyro_data);

  return 0;
}
