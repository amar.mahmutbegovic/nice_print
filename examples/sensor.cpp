#include "sensor.h"

#include "np.hpp"

void GyroData::print() const
{
  np::print("Printing gyro data: ", np::nl, x, np::nl, y, np::nl, z, np::nl);
}
